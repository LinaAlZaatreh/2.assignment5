import numpy as np
import matplotlib.pyplot as plt
from keras import layers
from keras.datasets import mnist
from keras.models import Model


def preprocess(images):
    images = images.astype("float32") / 255.0
    images = np.reshape(images, (len(images), 28, 28, 1))
    return images


def noise(images):
    noise_factor = 0.4
    noisy_images = images + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=images.shape)
    return np.clip(noisy_images, 0.0, 1.0)


def display(array1, array2):
    samples = 10

    indices = np.random.randint(len(array1), size=samples)
    images1 = array1[indices, :]
    images2 = array2[indices, :]

    plt.figure(figsize=(10, 2))
    for i, (image1, image2) in enumerate(zip(images1, images2)):
        ax = plt.subplot(2, samples, i + 1)
        plt.imshow(image1.reshape(28, 28))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        ax = plt.subplot(2, samples, i + 1 + samples)
        plt.imshow(image2.reshape(28, 28))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    plt.show()


(train_data, _), (test_data, _) = mnist.load_data()

train_data = preprocess(train_data)
test_data = preprocess(test_data)

noisy_train_data = noise(train_data)
noisy_test_data = noise(test_data)

display(train_data, noisy_train_data)

input_data = layers.Input(shape=(28, 28, 1))

x = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(input_data)
x = layers.MaxPooling2D((2, 2), padding='same')(x)
x = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = layers.MaxPooling2D((2, 2), padding="same")(x)

x = layers.Conv2DTranspose(64, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2DTranspose(64, (3, 3), strides=2, activation="relu", padding="same")(x)
x = layers.Conv2D(1, (3, 3), activation="sigmoid", padding="same")(x)

autoencoder = Model(input_data, x)
autoencoder.compile(optimizer="adam", loss="binary_crossentropy", metrics=["mse"])
autoencoder.summary()

autoencoder.fit(
    x=train_data,
    y=train_data,
    epochs=50,
    batch_size=128,
    shuffle=True,
    validation_data=(test_data, test_data),
)

predictions = autoencoder.predict(test_data)
display(test_data, predictions)

mse = autoencoder.evaluate(test_data, test_data)[1]
print("Mean Squared Error (MSE) on Test Data:", mse)

autoencoder.fit(
    x=noisy_train_data,
    y=train_data,
    epochs=50,
    batch_size=128,
    shuffle=True,
    validation_data=(noisy_test_data, test_data),
)

predictions = autoencoder.predict(noisy_test_data)
display(noisy_test_data, predictions)

mse = autoencoder.evaluate(test_data, predictions)[1]
print("Mean Squared Error (MSE) on Noisy Test Data:", mse)

autoencoder.save("mnist_autoencoder.h5")
